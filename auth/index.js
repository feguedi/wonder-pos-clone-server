const { compare } = require('bcrypt')
const { server } = require('hawk')

const users = {
    john: {
        username: 'john',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret'
        name: 'John Doe',
        id: '2133d32a'
    }
}

exports.getCredentialsFunc = id => users[id]

exports.validate = async (request, username, password, h) => {
    if (username == 'help')
        return { response: h.redirect('/help') }

    const user = users[username]
    if (!user)
        return { credentials: null, isValid: false }

    const isValid = await compare(password, user.password)
    const credentials = { id: user.id, name: user.name }

    return { isValid, credentials }
}

exports.validateHawk = async (req, res) => {
    let payload, status
    try {
        const { credentials, artifacts } = await server.authenticate(req, getCredentialsFunc)
        payload = `Hola, ${ credentials.name }`
        status = 200
    } catch (error) {
        payload = 'Error. '.concat(error.toString())
        status = 401
    }
}
