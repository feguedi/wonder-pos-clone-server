const Hapi = require('@hapi/hapi')
const { validate, getCredentialsFunc } = require('./auth')

const init = async () => {
    require('dotenv').config()

    const server = Hapi.server({
        port: process.env.PORT || 8080,
        host: '0.0.0.0'
    })

    await server.register(require('hawk'))

    if (process.env.NODE_ENV !== 'production') {
        const swagger = require('hapi-swagger')
        const inert = require('@hapi/inert')
        const vision = require('@hapi/vision')
        const pack = require('./package')

        const options = {
            info: {
                title: 'Wonder POS API documentation',
                version: pack.version
            }
        }

        await server.register([inert, vision, {
            plugin: swagger,
            options
        }])

        server.route(require('./routes/swagger'))
    }

    server.auth.strategy('default', 'hawk', { getCredentialsFunc })
    // server.auth.strategy('simple', 'hawk', { validate })
    // server.auth.default('simple')

    server.route(require('./routes'))

    await server.start()
    console.log('Server on', server.info.uri)
}

process.on('unhandledRejection', err => {
    console.error(err)
    process.exit(1)
})

init()
