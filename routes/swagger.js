const Joi = require('joi')

module.exports = [{
    method: 'GET',
    path: '/todo/{id}/',
    options: {
        handler: (req, h) => 'Todo'.concat(req.params.id),
        description: 'Get todo',
        notes: 'Returns a todo item by the id passed in the path',
        tags: ['api'], // ADD THIS TAG
        validate: {
            params: Joi.object({
                id : Joi.number()
                        .required()
                        .description('the id for the todo item'),
            })
        }
    },
}]