const routes = [
    {
        method: 'GET',
        path: '/',
        handler: (req, h) => {
            return 'Hola, mundo'
        }
    },
    {
        method: 'GET',
        path: '/auth',
        handler: (req, h) => {
            return 'Autenticado'
        }
    },
    {
        method: 'GET',
        path: '/help',
        handler: (req, h) => 'Sección de ayuda'
    }
]

module.exports = routes
